import React, {useState, createContext} from 'react';
import Input from './Components/Input/index';
import AddValue from './Components/Button';
import TodoList from './Components/TodoList';
import useLocalStorage from './hooks/useLocalStorage';
import EditModal from './Components/Modal/index';
import Pagination from './Components/Pagination/index'





 createContext({ todos: []});

function App() {
  
  const [tasks, setTasks] = useLocalStorage("todos", []);
  const [value, setValue] = useState('');
  const [show, setShow] = useState(false);
  const [editedElement, setEditedElement] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [tasksPerPage] = useState(5);


  const inputCb = (event) => setValue(event.target.value);



  // Добавление новой задачи

  const addNewTask = () => {
    if (value.length && value.length < 20) {
          setTasks([
      ...tasks,
      {id: Math.random(),title: value, done: false}
    ])
    setValue('');
    }
  };



  // Выполнение задачи

  const changeTaskStatus = el => {
    setTasks(tasks.map(item => item.id === el.id ?{...item, done: !item.done}:item))
  }



  // Удаление задачи

  const deleteTask = id => {
    const filterItem = tasks.filter(todo => todo.id !== id);
    setTasks(filterItem)
  }



  // Изменение задачи

  const openModal = (el) => {

    setEditedElement(el);
    setShow(true)
  };

  // const editOldTask = (id, value) => {

  // }
  const closeModal = (id, value) => {

    setTasks(tasks.map(item => item.id === id ? {...item, title: value}:item))
    setShow(false)
  };
  
  const indexOfLastTask = currentPage * tasksPerPage;
  const indexOfFirstTask = indexOfLastTask - tasksPerPage;
  const currentTasks = tasks.slice(indexOfFirstTask, indexOfLastTask)

    const paginate = (pageNum) => setCurrentPage(pageNum);

  return (
    <div className="container">
      <div className="box add-box">
      <Input autoFocus={true} placeholder="Add new todo" value={value} handler={inputCb}/>
      <AddValue onClick={addNewTask}>Add todo</AddValue>
      </div>
      <div className="box">
      <TodoList tasks={currentTasks} deleteTask={deleteTask} openModal={openModal} changeTaskStatus={changeTaskStatus}/>
      <EditModal closeModal={closeModal} show={show} editedElement={editedElement} />
      <Pagination  paginate={paginate} tasksPerPage={tasksPerPage} totalTasks={tasks.length}/>
      </div>
      
    </div>
  )
}


export default App;
