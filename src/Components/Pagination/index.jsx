import React from 'react'

 function Pagination(props) {
    
    const {tasksPerPage, totalTasks, paginate} = props;

    const pageNumbers = [];

    for(let i = 1; i <= Math.ceil(totalTasks / tasksPerPage); i++)
        pageNumbers.push(i);
        // currentTasks();
    return (
        <div>
            <ul className="pagination justify-content-center">
                    {pageNumbers.map(num => (
                        <li className="page-item" key={num}>
                            <button onClick={() => paginate(num)}  className="page-link">{num}</button>
                        </li>
                    ))}
                </ul>
        </div>
    )
}


export default Pagination;