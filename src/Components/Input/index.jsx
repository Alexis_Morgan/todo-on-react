import React from 'react';
import './index.css'


const Input = (props) => {

    const {value, placeholder, handler} = props;

    return (
        <input
         value={value}
         autoFocus
         placeholder={placeholder}
         onChange={(event) => handler(event)}
        />
    )

}
export default Input