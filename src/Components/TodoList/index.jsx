import React from 'react';
import './index.css';
import DeleteTodo from '../DeleteTodo/index'
import EditTodo from '../EditTodo';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCheckSquare, faSquare} from '@fortawesome/free-regular-svg-icons'
// import BootstrapTable from 'react-bootstrap-table-next';
// import paginationFactory from 'react-bootstrap-table2-paginator';

const TodoList = (props) => {

  const {tasks, deleteTask, openModal, changeTaskStatus} = props;
  
//   <BootstrapTable
//   keyField="name"
//   data={tasks}
//   columns={columns}
//   pagination={paginationFactory()}    
//  />
  
  return (
    <ul className='tasks-list'>
      {tasks.map((el) => (
        
      <li key={el.id}>
        <FontAwesomeIcon icon={el.done ? faCheckSquare: faSquare} onClick={() => changeTaskStatus(el)}/>
        {el.title}
        <div>
        <EditTodo onClick={() => openModal(el)}/>
        <DeleteTodo onClick={() => deleteTask(el.id)}/>
        </div>
      </li>
      ))}
    </ul> 
  )
}

  export default TodoList;