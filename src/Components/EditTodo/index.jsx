import React from 'react'
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import IconButton from '@material-ui/core/IconButton';


const EditTodo = (props) => {
    return(
        <IconButton onClick={props.onClick} color="primary" aria-label="edit"><EditOutlinedIcon /></IconButton>

    )

}

export default EditTodo
