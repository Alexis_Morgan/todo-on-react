import React from 'react';
import 'typeface-roboto';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';


const DeleteTodo = (props) => {
    return(
        <IconButton color="secondary" aria-label="delete" onClick={props.onClick}><DeleteIcon /></IconButton>
    )
}


export default DeleteTodo;