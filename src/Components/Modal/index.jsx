import React, { useState, useEffect } from 'react';
import SaveIcon from '@material-ui/icons/Save';
import IconButton from '@material-ui/core/IconButton';
import './index.css';
import Modal from 'react-modal';


const customStyles = {
  content : {
    top                   : '20%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
  }
};


Modal.setAppElement('#root')



const EditModal = (props) => {
    
    const {show, closeModal, editedElement} = props;

    const editInputCb = (event) => setValueEditInput(event.target.value);
    // console.log(editedElement)
    const [valueEditInput, setValueEditInput] = useState('');
    const [taskOldId, setTaskOldId] = useState('');


      useEffect(() => {
      if (editedElement) setValueEditInput(editedElement.title)
      }, [editedElement]
      );

      useEffect(() => {
        if (editedElement) setTaskOldId(editedElement.id)
        }, [editedElement]
        )

    return(
      <Modal className=""
      isOpen={show}
      style={customStyles}
      >
        <div className={show ? "modal" : "hide"}>
          <form className="edit">
            <h2>Edit todo</h2>
              <div className="edit-div">
                <input 
                value={valueEditInput}
                onChange={(event) => editInputCb(event)}
                />
                <IconButton onClick={() => {
                 closeModal(taskOldId, valueEditInput)
                }
                  } color="primary"><SaveIcon style={{ fontSize: 32 }}/></IconButton>
              </div>
          </form>
        </div>
        </Modal>
        
    )
    
}

export default EditModal;