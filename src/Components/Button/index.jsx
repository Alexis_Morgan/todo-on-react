import React from 'react';
import './index.css'
import 'typeface-roboto';
import Button from '@material-ui/core/Button';

const AddValue = (props) => {

    return (
      <Button variant="contained" color="primary" onClick={props.onClick}>Add value</Button>
  )
}
    
  export default AddValue;